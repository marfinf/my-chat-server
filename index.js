// server.js

const express = require('express');
const fs = require('fs');
const cors = require('cors'); // Import the cors package
const { Document, Packer, Paragraph, HeadingLevel, TextRun, TableOfContents } = require('docx');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const port = 3000;

app.use(cors()); // Use the cors middleware
app.use(bodyParser.json());

app.post('/generate-doc', (req, res) => {
    console.log("triggered")
    console.log(req)
    const { inputString } = req.body;

    // Function to parse the input string and create document content
    function parseInput(input) {
        const lines = input.split('\n');
        const children = [];
        let isAfterTOC = true;

        lines.forEach(line => {
            if (line.startsWith('# ')) {
                children.push(new Paragraph({
                    children: [
                        new TextRun({
                            text: line.replace('# ', ''),
                            bold: true,
                        }),
                    ],
                    heading: HeadingLevel.TITLE,
                    style: "titleStyle"
                }));
                children.push(new Paragraph({
                    text: 'Table of Contents',
                    heading: HeadingLevel.HEADING_1,
                    thematicBreak: true,
                    pageBreakBefore: true,
                }),
                    new TableOfContents({
                        hyperlink: true,
                        heading: 'Table of Contents',
                        styles: {
                            paragraphStyle: 'TOCHeading',
                            level: 3,
                        },
                    }))
            } else if (line.startsWith('## ')) {
                children.push(new Paragraph({
                    children: [
                        new TextRun({
                            text: line.replace('## ', ''),
                            bold: true,
                            color: '000000',
                        }),
                    ],
                    heading: HeadingLevel.HEADING_1,
                    style: "heading1Style",
                    pageBreakBefore: isAfterTOC
                }));
                isAfterTOC = false
            }
            else if (line.startsWith("### ")) {
                children.push(new Paragraph({
                    children: [new TextRun('')],
                }));
                children.push(new Paragraph({
                    children: [
                        new TextRun({
                            text: line.replace('### ', ''),
                            bold: true,
                            color: '000000',
                        }),
                    ],
                    heading: HeadingLevel.HEADING_1,
                    style: "heading1Style",
                    pageBreakBefore: isAfterTOC,
                }));
                isAfterTOC = false
            }
            else if (line.trim() !== '') {
                children.push(new Paragraph({
                    children: [
                        new TextRun({
                            text: line,
                        }),
                    ],
                    style: "normalStyle"
                }));
            }
            else {
                // Handle new lines
                children.push(new Paragraph({
                    children: [
                        new TextRun({
                            text: '',
                        }),
                    ],
                    style: "normalStyle"
                }));
            }
        });

        return children;
    }

    // Create a new document
    const doc = new Document({
        sections: [
            {
                properties: {},
                children: parseInput(inputString),
            },
        ],
        styles: {
            paragraphStyles: [
                {
                    id: "titleStyle",
                    name: "Title",
                    basedOn: "Normal",
                    next: "Normal",
                    quickFormat: true,
                    run: {
                        size: 48, // Font size 24 points (48 half-points)
                        bold: true,
                    },
                },
                {
                    id: "heading1Style",
                    name: "Heading 1",
                    basedOn: "Normal",
                    next: "Normal",
                    quickFormat: true,
                    run: {
                        size: 32, // Font size 16 points (32 half-points)
                        bold: true,
                    },
                },
                {
                    id: "normalStyle",
                    name: "Normal",
                    basedOn: "Normal",
                    next: "Normal",
                    quickFormat: true,
                    run: {
                        size: 24, // Font size 12 points (24 half-points)
                    },
                },
            ],
        },
    });

    // Pack the document and save it as a .docx file
    Packer.toBuffer(doc).then((buffer) => {
        fs.writeFileSync("output.docx", buffer);
        res.send("Document created successfully.");
    });
});

app.get('/download-doc', (req, res) => {
    const filePath = path.join(__dirname, 'output.docx');
    res.download(filePath, 'output.docx', (err) => {
        if (err) {
            res.status(500).send({ error: "Error downloading the file." });
        }
    });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
