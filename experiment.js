const axios = require('axios');

async function getChatCompletion(messageHist) {
    try {
        const response = await axios.post(
            'https://api.openai.com/v1/chat/completions',
            {
                model: 'gpt-4o',
                messages: messageHist,
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer sk-proj-z6jTl7CyMkRHo4aUtFU2T3BlbkFJB5zXIYTTvtrAubyWoP3B`,
                },
            }
        );
        return response.data;
    } catch (error) {
        console.error('Error fetching chat completion:', error);
        throw error;
    }
}

// Example usage:
const messageHist = [
    { role: 'user', content: 'Hello, who won the world series in 2020?' }
];

getChatCompletion(messageHist).then(result => {
    console.log('Chat completion result:', result.choices[0].message);
}).catch(error => {
    console.error('Error:', error);
});
