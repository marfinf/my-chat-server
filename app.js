const express = require('express');
const path = require('path');
const fs = require('fs');
const mammoth = require('mammoth');
const TurndownService = require('turndown');
const MarkdownIt = require('markdown-it');
const htmlToDocx = require('html-to-docx');

const axios = require('./lib/axiosInstance');
const setupApp = require('./lib/setup');
const {
    addTableRule,
    addMessageToHashMap,
    hashmap,
    logErrorToFile,
    upload,
    getUploadedFileName
} = require('./lib/utils');


const app = express();
const port = 3001;

setupApp(app)


const handleError = (errorMsg, res) => {
    logErrorToFile(errorMsg)
    if (res) res.status(500).send('Internal Server Error');
}

const getChatCompletion = async (key, requestToAI) => {
    try {
        const requestMessage = { role: 'user', content: requestToAI }
        addMessageToHashMap(key, requestMessage)
        const result = await axios.post(
            'https://api.openai.com/v1/chat/completions',
            {
                model: 'gpt-4o',
                messages: hashmap[key]
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer sk-proj-z6jTl7CyMkRHo4aUtFU2T3BlbkFJB5zXIYTTvtrAubyWoP3B`,
                },
            }
        );
        if (result.data.choices[0].message.content && result.data.choices[0].message.content.length > 0) {
            const responseMessage = { role: 'assistant', content: result.data.choices[0].message.content }
            addMessageToHashMap(key, responseMessage)

            return result.data.choices[0].message.content
        } else {
            return ""
        }
    } catch (error) {
        handleError(error)
        return ""
    }
}

const convertDocxToMarkdown = async (docxFilePath) => {
    try {
        // Read the DOCX file
        const buffer = fs.readFileSync(docxFilePath);

        // Convert DOCX to HTML
        const { value: html } = await mammoth.convertToHtml({ buffer });

        // Initialize TurndownService and add custom rules
        const turndownService = new TurndownService();
        addTableRule(turndownService);

        // Convert HTML to Markdown
        let markdown = turndownService.turndown(html);
        markdown = markdown.replace(/(\d+)\\\. /g, '## $1. ');

        return markdown;
    } catch (error) {
        handleError(error)
    }
};

const generateResult = async (key, result, res) => {
    try {
        const markdownParts = result.split('<!-- PAGE BREAK -->');
        const md = new MarkdownIt();
        const htmlParts = markdownParts.map(part => md.render(part));
        const pageBreak = '<div class="page-break" style="page-break-after: always;"></div>';
        const mergedHtmlContent = htmlParts.join(pageBreak);
        const docxBuffer = await htmlToDocx(mergedHtmlContent);
        const outputPath = path.join('generate/', `${key} - output.docx`);
        fs.writeFileSync(outputPath, docxBuffer);
        res.send(result);
    } catch (error) {
        handleError(error)
    }
}


app.post('/upload', upload.single('file'), async (req, res) => {
    try {
        const file = req.file;
        const key = req.query.key;
        const input = req.query.input;

        if (!file) {
            return res.status(400).send({ message: 'Please upload a file' });
        }

        const docxFilePath = `uploads/${getUploadedFileName()}`;
        const markdown = await convertDocxToMarkdown(docxFilePath);
        const requestToAI = `${input}\n${markdown}`

        const answer = await getChatCompletion(key, requestToAI)
        await generateResult(key, answer, res)
    } catch (error) {
        handleError(error, res)
    }
});

app.post('/generate-doc', async (req, res) => {
    try {
        const { key, inputString } = req.body;

        const requestToAI = `${inputString}. Please create 1/2 from full content with rules below:
            1. If you want to make a new page or page break please add \`<!-- PAGE BREAK -->\`
            2. Don't include your comment like thanks, etc. Just provide the ANSWER ONLY.
        `
        const requestToAI2 = "continue 2/2"

        const firstAnswer = await getChatCompletion(key, requestToAI)

        if (!firstAnswer || firstAnswer === "") {
            throw "We are experiencing A.I Technical Difficulties";
        }

        const secondAnswer = await getChatCompletion(key, requestToAI2)

        if (!secondAnswer || secondAnswer === "") {
            throw "We are experiencing A.I Technical Difficulties";
        }

        const result = `${firstAnswer}\n${secondAnswer}`
        await generateResult(key, result, res);
    }
    catch (error) {
        handleError(error, res)
    }
});

app.get('/download-doc', (req, res) => {
    try {
        const { key } = req.query;
        const docFile = `generate/${key} - output.docx`
        const filePath = path.join(__dirname, docFile);

        res.download(filePath, docFile, (err) => {
            if (err) {
                res.status(505).send({ error: `Error download: ${err}` });
                logErrorToFile(err)
            }
        });
    } catch (err) {
        handleError(err, res)
    }
});

app.listen(port, () => { });
