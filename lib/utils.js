const path = require('path');
const fs = require('fs');
const multer = require('multer');


const hashmap = {};
let fileused = '';

const logDirectory = path.join(__dirname, '../logs');
if (!fs.existsSync(logDirectory)) {
    fs.mkdirSync(logDirectory);
}

const addTableRule = (turndownService) => {
    turndownService.addRule('table', {
        filter: 'table',
        replacement: (content, node) => {
            const rows = Array.from(node.querySelectorAll('tr'));
            let tableMarkdown = '';

            rows.forEach((row, rowIndex) => {
                const cells = Array.from(row.querySelectorAll('th, td'));
                const cellMarkdown = cells.map(cell => cell.textContent.trim()).join(' | ');

                if (rowIndex === 0) {
                    tableMarkdown += `| ${cellMarkdown} |\n`;
                    tableMarkdown += `| ${cells.map(() => '---').join(' | ')} |\n`;
                } else {
                    tableMarkdown += `| ${cellMarkdown} |\n`;
                }
            });

            return `\n${tableMarkdown}\n`;
        }
    });
};


function addMessageToHashMap(key, messageObject) {
    if (!hashmap[key]) {
        hashmap[key] = [];
    }
    hashmap[key].push(messageObject);
}

function logErrorToFile(error) {
    const logFilePath = path.join(logDirectory, 'error.log');

    const errorMessage = `${new Date().toISOString()} - Error: ${error.message}\nStack: ${error.stack}\n\n`;

    fs.appendFile(logFilePath, errorMessage, (err) => {
        if (err) {
            console.error('Failed to write to log file:', err);
        } else {
            console.log('Error logged to file:', logFilePath);
        }
    });
}

function logInfoToFile(message, additionalInfo = {}) {
    const logFilePath = path.join(logDirectory, 'server.log');
    const logMessage = `${new Date().toISOString()} - Info: ${message}\nAdditional Info: ${JSON.stringify(additionalInfo)}\n\n`;

    fs.appendFile(logFilePath, logMessage, (err) => {
        if (err) {
            console.error('Failed to write to log file:', err);
        } else {
            console.log('Info logged to file:', logFilePath);
        }
    });
}

function getUploadedFileName() {
    return fileused
}

// Set up multer for file uploads
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const uploadDir = 'uploads/';
        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir);
        }
        cb(null, uploadDir);
    },
    filename: (req, file, cb) => {
        fileused = `${req.query.key} - ${file.originalname}`
        cb(null, fileused);
    },
});

const upload = multer({ storage });

module.exports = {
    addMessageToHashMap,
    logErrorToFile,
    logInfoToFile,
    upload,
    addTableRule,
    getUploadedFileName,
    hashmap
};

