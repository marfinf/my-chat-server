const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const express = require('express');


const logDirectory = path.join(__dirname, '../logs');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

const accessLogStream = fs.createWriteStream(path.join(logDirectory, 'client.log'), { flags: 'a' });


module.exports = (app) => {
    app.use(cors());
    app.use(bodyParser.json());

    app.use(morgan('combined', { stream: accessLogStream }));
    app.use((req, res, next) => {
        accessLogStream.write(`Request Body: ${JSON.stringify(req.body)}\n`);
        const originalSend = res.send;

        res.send = function (body) {
            accessLogStream.write(`Response Body: ${body}\n`);
            originalSend.call(this, body);
        };
        next();
    });

    app.use('/uploads', express.static(path.join(__dirname, '../uploads')));
};