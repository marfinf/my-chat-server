const axios = require('axios');
const { logInfoToFile, logErrorToFile } = require('./utils');


const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(request => {
    logInfoToFile(`Outgoing request: ${request.method.toUpperCase()} ${request.url}`, { data: request.data });
    return request;
}, error => {
    logErrorToFile(error);
    return Promise.reject(error);
});

axiosInstance.interceptors.response.use(response => {
    logInfoToFile(`Incoming response: ${response.status} ${response.statusText}`, { data: response.data });
    return response;
}, error => {
    logErrorToFile(error);
    return Promise.reject(error);
});


module.exports = axiosInstance;
