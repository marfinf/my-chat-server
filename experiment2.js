const axios = require('axios');
const { addMessageToHashMap, hashmap, logErrorToFile, upload, fileused } = require('./utils');


async function getChatCompletion(key, requestToAI) {
    try {
        const requestMessage = { role: 'user', content: requestToAI }
        addMessageToHashMap(key, requestMessage)

        const responseMessage = { role: 'assistant', content: "AI Answer" }
        addMessageToHashMap(key, responseMessage)

        return "unit test"
    } catch (error) {
        console.error('Error fetching chat completion:', error);
        logErrorToFile(error);
        return ""
    }
}


getChatCompletion("1720403919", "user question").then(result => {
    console.log('Chat completion result:', result);
}).catch(error => {
    console.error('Error:', error);
});

