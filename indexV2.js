const express = require('express');
const bodyParser = require('body-parser');
const MarkdownIt = require('markdown-it');
const htmlToDocx = require('html-to-docx');
const fs = require('fs');
const cors = require('cors'); // Import the cors package


const app = express();
const port = 3001;

app.use(cors()); // Use the cors middleware
app.use(bodyParser.json());

app.post('/generate-doc', async (req, res) => {
    const { inputString } = req.body;

    const markdownParts = inputString.split('<!-- PAGE BREAK -->');
    const md = new MarkdownIt();
    const htmlParts = markdownParts.map(part => md.render(part));
    const pageBreak = '<div class="page-break" style="page-break-after: always;"></div>';
    const mergedHtmlContent = htmlParts.join(pageBreak);

    try {
        const docxBuffer = await htmlToDocx(mergedHtmlContent);
        fs.writeFileSync('output.docx', docxBuffer);
        res.send(docxBuffer);
    } catch (error) {
        console.error('Error converting markdown to docx:', error);
        res.status(500).send('Internal Server Error');
    }
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
